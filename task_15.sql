-- 1.5
SELECT ship.name, sc.name as class
FROM ship
         JOIN ship_class sc ON ship.class_id = sc.id
order by ship.name;

-- 1.19
SELECT *
FROM ship_class
WHERE country = 'Japan'
ORDER BY type DESC;

-- 1.20
SELECT name, launch_year AS lauched
FROM ship
WHERE launch_year BETWEEN 1920 AND 1942
ORDER BY lauched DESC;

-- 1.21
SELECT ship.name as ship, battle.name AS battle, outcome_result.name as result
FROM ship
         JOIN outcome ON ship.id = outcome.ship_id
         JOIN battle ON outcome.battle_id = battle.id
         JOIN outcome_result ON outcome.result_id = outcome_result.id
WHERE outcome_result.name <> 'Sunk'
ORDER BY ship.name DESC;

-- 1.22
SELECT ship.name as ship, battle.name AS battle, outcome_result.name as result
FROM ship
         JOIN outcome ON ship.id = outcome.ship_id
         JOIN battle ON outcome.battle_id = battle.id
         JOIN outcome_result ON outcome.result_id = outcome_result.id
WHERE outcome_result.name = 'Sunk'
ORDER BY ship.name DESC;

-- 1.23
SELECT name, displacement FROM ship_class
WHERE displacement >= 40000
ORDER BY type;

-- 2.4
SELECT name as ship FROM ship
WHERE ship.name LIKE 'W%n';

-- 2.5
SELECT name as ship FROM ship
WHERE lower(ship.name) LIKE '%e%e%';

-- 2.6
SELECT name as ship, launch_year as launched FROM ship
WHERE ship.name NOT LIKE '%a';

-- 3.5
SELECT country, name, type
FROM ship_class as outt
WHERE (SELECT count(id) FROM ship_class as inne WHERE inne.country = outt.country AND inne.type = 'bb') > 0
AND (SELECT count(id) FROM ship_class as inne WHERE inne.country = outt.country AND inne.type = 'bc') > 0