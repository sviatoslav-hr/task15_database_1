CREATE TABLE IF NOT EXISTS ship_class
(
    id           INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name         VARCHAR(50)     NOT NULL,
    type         VARCHAR(2)      NOT NULL,
    country      VARCHAR(20)     NOT NULL,
    guns_num     INT,
    bore         FLOAT,
    displacement int
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS ship
(
    id          INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name        varchar(50)     NOT NULL,
    launch_year INT             NOT NULL,
    class_id    INT             NOT NULL REFERENCES ship_class (id)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS battle
(
    id   INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(20)     NOT NULL,
    date DATETIME        NOT NULL
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS outcome_result
(
    id   INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(10)     NOT NULL
) ENGINE = InnoDB;